package spellingbee.utilities;
/**
 * a collection of utility methods

 * 
 * @author Mishal Alarifi
 *
 */
public class Utilities {
	/*
	 * @param int[] the array to be turned into a String representation
	 * 
	 * @return String String representation of all the elements of the array separated by a space
	 */
	public static String intArrToString(int[] arr) {
		String arrString = "";
		for (int num : arr) {
			arrString += String.valueOf(num) + " ";
		}
		return arrString;
	}
	
	/*
	 * @param String String representing an array from the intArrToString method
	 * 
	 * @return int[] array built from it's String representation
	 */
	public static int[] stringToIntArr(String arrString) {
		String[] arr = arrString.split(" ");
		int[] intArr = new int[arr.length];
		for (int i = 0; i < arr.length; i++) {
			intArr[i] = Integer.valueOf(arr[i]);
		}
		return intArr;
	}
}
