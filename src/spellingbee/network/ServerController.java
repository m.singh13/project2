package spellingbee.network;

import spellingbee.client.ISpellingBeeGame;
import spellingbee.client.SimpleSpellingBeeGame;
import spellingbee.client.SpellingBeeGame;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	
	
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	
	//private ISpellingBeeGame simpleSpellingBee = new SimpleSpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {

		if(inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		else if(inputLine.equals("getCenter")) {
			char center =spellingBee.getCenterLetter();
			String centerLetter = String.valueOf(center);
		
			return centerLetter;
		}
		else if(inputLine.contains("wordCheck")) {
		   String words[] = inputLine.split(";");
		   String checkWordMessage = spellingBee.getMessage(words[1]);
		   String points = String.valueOf(spellingBee.getScore());
		   return(checkWordMessage+";"+points);
		}
		else if(inputLine.equals("getCurrentScore")){
		   return(String.valueOf(spellingBee.getScore()));
		}
		else if(inputLine.equals("getBrackets")) {
			return String.valueOf(spellingbee.utilities.Utilities.intArrToString(((SpellingBeeGame) spellingBee).bracket));
		}
			return "PROTOCOL DOES NOT EXIST";
	}
	
	
}
