package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import spellingbee.network.Client;
/**
 * This class is incharge of the GameTab GUI
 * @author Mandeep Singh
 *
 */
public class GameTab extends Tab{
	//Private fields
	private Client gameClient;
	private TextField points;
	//Constructors
	public GameTab(Client gameClient) {
		super("Game");
		this.gameClient = gameClient;
		createGameTab();
	}
	/**
	 *  This method creates the GameTab with the necessary functionality
	 */
	public void createGameTab() {
		VBox gameTab = new VBox();
		//Input Textfield
		TextField input = new TextField("");
		//Letter Buttons
		HBox buttons = createLetterButtons(input);
		//Clear Button
		Button clear = createClearButton(input);
		//Delete Button
		Button delete = createDeleteButton(input);
		buttons.getChildren().addAll(clear,delete);
		//Word TextField
		HBox textfield = new HBox();
		textfield.getChildren().add(input);
		//Message and points TextField
		HBox messagePoints = new HBox();
		TextField message = new TextField("");
		message.setPrefWidth(265);
		this.points = new  TextField("0");
		//Submit Button
		Button submit =  createSubmitButton(input,message,points,gameClient);
		messagePoints.getChildren().addAll(message,points);
		
		gameTab.setStyle("-fx-background-color:black;-fx-pref-height:1000px;-fx-pref-width:1000px;");
		gameTab.getChildren().addAll(buttons,textfield,submit,messagePoints);
		
	    this.setContent(gameTab);	
	}
	/**
	 *  method creates all letter buttons being used in the SpellingBeeClient
	 *  if its the center letter set the style to red
	 *  
	 * @param input Textfield for the word
	 * @return HBox containing 7 letter buttons
	 */
	public HBox createLetterButtons(TextField input) {
		HBox buttons = new HBox();
		String letters[] = this.gameClient.sendAndWaitMessage("getAllLetters").split("");
		char centerLetter = this.gameClient.sendAndWaitMessage("getCenter").charAt(0);
		
		for(int i=0; i<letters.length;i++) {
			
		  if(letters[i].charAt(0) == centerLetter) {
			  Button btn = createButton(letters[i],input);
			  btn.setStyle("-fx-text-fill:red;");
			  buttons.getChildren().add(btn);
		  }
		  
		  else {
			  Button btn = createButton(letters[i],input);
			  buttons.getChildren().add(btn);
		  }  
		  
		  
		}
		return buttons;	
	}
	/**
	 *  method creates a button with a letter.
	 *  Adds EventHandler ActionEvent, when the button is clicked set the input TextField to the corresponding letter button.
	 * 
	 * @param letter String representing the letter
	 * @param input Textfield for the word
	 * @return Button with letter
	 */
	
	public Button createButton(String letter,TextField input) {
		
		Button btn = new Button(letter);
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				String newInput = input.getText();
				input.setText(newInput +btn.getText());
			}
	
		  });
		
		return btn;	
	}
	/**
	 * method creates a clear button.
	 * Adds EventHandler ActionEvent, when the button is clicked set the input TextField empty.
	 * 
	 * @param input Textfield for the word
	 * @return Button that clears the input
	 */
	
	public Button createClearButton(TextField input) {
		Button clear = new Button("CLEAR");
		clear.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				input.setText("");
			}
		});
		return clear;
	}
	/**
	 * method creates a delete button.
	 * Adds EventHandler ActionEvent, when the button is clicked remove the last letter in the input Textfield.
	 * 
	 * @param input Textfield for the word
	 * @return Button that deletes last character from input
	 */
	
	public Button createDeleteButton(TextField input) {
		
		Button delete = new Button("DELETE");
		delete.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String currentInput = input.getText();
				if(currentInput != null && currentInput.length() > 0) {
				currentInput = currentInput.substring(0,currentInput.length()-1);
				input.setText(currentInput);
				}
			}
		});
		return delete;
	}
	/**
	 * method creates submit button.
	 * Add EventHandler ActionEvent, from SubmitHandler class.
	 * 
	 * @param input Textfield for the word
	 * @param message Textfield for the message
	 * @param points Textfield for the player's score
	 * @param clientObject Client to communicate with Server
	 * @return Button that submits request to server
	 */
	public Button createSubmitButton(TextField input,TextField message, TextField points, Client clientObject) {

		Button submit = new Button("SUBMIT");
		submit.setOnAction(new SubmitHandler(input,message,points,clientObject));
		return submit;	
	}
	
	public TextField getScoreField() {
		return this.points;
	}
	
	
}
