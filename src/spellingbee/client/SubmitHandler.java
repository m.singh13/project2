package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import spellingbee.network.Client;

/**
 * This class incharge of the submit button in the GUI and implements EventHandler ActionEvent
 * 
 * @author Mandeep Singh
 *
 */

public class SubmitHandler implements EventHandler<ActionEvent> {
	
	//Private fields
	private TextField input;
	private TextField message;
	private TextField score;
	private Client gameClient;
	
	//Constructor with 4 fields
	public SubmitHandler(TextField input, TextField message, TextField score, Client gameClient) {
		this.input = input;
		this.message = message;
		this.score = score;
		this.gameClient = gameClient;
		
	}
	/**
	 * This method handles the event when the player clicks submit button
	 * It verifies if the word is valid or not and sends appropriate message and score on the GUI
	 */
	public void handle(ActionEvent e) {
		String wordCheck = gameClient.sendAndWaitMessage("wordCheck;"+this.input.getText().toLowerCase());
	    String[] serverFeedback = wordCheck.split(";");
	    this.message.setText(serverFeedback[0]);
	    this.score.setText(serverFeedback[1]); 

	}
}