package spellingbee.client;
/**
 * 
 * @author Mandeep Singh
 * 
 * This interface is incharge of storing necessary values of the SpellingBeeGame
 *
 */

public interface ISpellingBeeGame {
	/**
	 * Finds the points given to the word
	 * @param attempt The String word
	 * @return number of points that a given word is worth according to the Spelling Bee Rules
	 */
	 int getPointsForWord(String attempt);
	 
	 /**
	  * Checks if the word is valid or not and returns appropriate message.
	  * 
	  * @param attempt The String word
	  * @return  message based on word validation
	  */
	 String getMessage(String attempt);
	 
	 /**
	  * 
	  * @return string of all 7 letters used in the game
	  */
	 
	 String getAllLetters();
	 
	 /**
	  * 
	  * @return char of the center letter used in the game
	  */
	 
	 char getCenterLetter();
	 
	 /**
	  * 
	  * @return value of the player's score
	  */
	 
	 int getScore();
	 
	 /**
	  * 
	  * @return array of numbers based on the points category
	  */
	 
	 int[] getBrackets();
	  
}
