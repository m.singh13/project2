package spellingbee.client;

/**
 * 
 * @author  Mishal Alarifi
 * 
 * This class draws the score tab and manages it's changes
 *
 */

import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

public class ScoreTab extends Tab {
	private Client clientObj;
	private int score;
	private String[] brackets;
	private Text currentScore;
	
	/*
	 * constructor which gets brackets from the clientObj
	 * 
	 * @param Client object that this is drawing for
	 */
	public ScoreTab(Client clientObj) {
		super("Score");
		this.clientObj = clientObj;
		brackets = clientObj.sendAndWaitMessage("getBrackets").split(" ");
		createScoreTab();
	}
	
	/*
	 * draws the score tab and manages it's colours based on the score
	 * 
	 */
	public void createScoreTab() {
		VBox scoreTab = new VBox();
		GridPane gp = new GridPane();
		Text queen = new Text("Queen Bee");
		Text queenScore = new Text(brackets[4]);
		Text genius = new Text("Genius");
		Text geniusScore = new Text(brackets[3]);
		Text amazing = new Text("Amazing");
		Text amazingScore = new Text(brackets[2]);
		Text good = new Text("Good");
		Text goodScore = new Text(brackets[1]);
		Text start = new Text ("Getting started");
		Text startScore = new Text(brackets[0]);
		Text current = new Text("Current Score");
		currentScore = new Text(String.valueOf(score));
		currentScore.setFill(Color.RED);
		start.setFill(Color.GREY);
		good.setFill(Color.GREY);
		amazing.setFill(Color.GREY);
		genius.setFill(Color.GREY);
		queen.setFill(Color.GREY);
		
		if(score >= Integer.parseInt(brackets[0])) {
			start.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[1])) {
			good.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[2])) {
			amazing.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[3])) {
			genius.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[4])) {
			queen.setFill(Color.BLACK);
		}
		
		gp.add(queen,0,0,1,1);
		gp.add(queenScore,1,0,1,1);
		gp.add(genius,0,1,1,1);
		gp.add(geniusScore,1,1,1,1);
		gp.add(amazing,0,2,1,1);
		gp.add(amazingScore,1,2,1,1);
		gp.add(good,0,3,1,1);
		gp.add(goodScore,1,3,1,1);
		gp.add(start,0,4,1,1);
		gp.add(startScore,1,4,1,1);
		gp.add(current,0,5,1,1);
		gp.add(currentScore,1,5,1,1);
		gp.setHgap(10);
		scoreTab.getChildren().add(gp);
		this.setContent(scoreTab);
	}
	

	/*
	 * method which refreshes the score tab and updates all the values
	 * 
	 */
	public void refresh() {
		score = Integer.parseInt(clientObj.sendAndWaitMessage("getCurrentScore"));
		currentScore.setText(String.valueOf(score));
		createScoreTab();
	}
	
}
