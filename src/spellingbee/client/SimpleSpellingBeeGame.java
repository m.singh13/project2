package spellingbee.client;
/**
 * This class is in charge of creating the GUI and implements the ISpellingBeeGame interface
 * It is a simple version of the game to make sure everything works.
 * 
 * @author Mandeep Singh
 *
 */

public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	
	 //Private fields
	  private String letters;
	  private char centerLetter;
	  private int score = 0;
	  
	//Constructor
	public SimpleSpellingBeeGame() {
		this.letters ="ENYITAM";
		this.centerLetter = letters.charAt(3);	
	}
	
	public int getPointsForWord(String attempt) {
		int points = 0;
		
		if(attempt.length() == 4) {
			points = 1;
		}
		
		if(attempt.length() >4) {
			points = attempt.length();
		}
		
		if(attempt.length() == 7) {
			points = 14;
		}
		
		return points;
	}
	
	public String getMessage(String attempt) {
		
		String message ="";
		
		if ((attempt.contains(String.valueOf(this.centerLetter)))&&(attempt.matches("^["+this.letters+"]+$"))&&checkWord(attempt)) {
			
			message ="Good Word!";
			this.score +=getPointsForWord(attempt);
			
		}
		
		else if(!(attempt.contains(String.valueOf(this.centerLetter)))){
			
			message = "Bad Word, not using center letter";
			
		}
		
		else if(!(attempt.matches("^["+this.letters+"]+$"))) {
			message = "Bad Word, use letters provided";
		}
		
		else if(!(checkWord(attempt))) {
			message = "Bad Word, made up word";
		}
		
		else {
			message = "ERROR";
		}
		
		return message;
	}
	
	public String getAllLetters() {
		
		return this.letters;
		
	}
	
	public char getCenterLetter() {
		return this.centerLetter;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public int[] getBrackets() {
		int[]  brackets = {1,2,3,4,5};
		return brackets;
	}
	/**
	 *  Checks if the word is a real word or not.
	 * @param attempt The String word
	 * @return boolean value based on the words validity 
	 */
	public boolean checkWord(String attempt) {
		 String[] validWords = {"ANYTIME","INMATE","MINTY","YETI","MINE","TIME","TIE"};
		 
		 for(int i =0; i<validWords.length;i++) {
			 if(validWords[i].equals(attempt)) {
				 return true;
			 }
			 
		 }
		return false;
	}
	
	
	

}
