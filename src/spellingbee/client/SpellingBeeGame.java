package spellingbee.client;
/**
 * the functionality of the game itself is contained within this class
 * 
 * @author Mishal Alarifi
 *
 */
import java.io.IOException;
import java.util.Random;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SpellingBeeGame implements ISpellingBeeGame {
	
	private String letters;
	private char centreLetter;
	private int score;
	private ArrayList<String> wordsFound;
	private static final HashSet<String> ALLWORDS = createWordsFromFile("DataFiles/english.txt");
	public int[] bracket;
	private final int NUMLINES = 500;
	
	/*
	 * constructor which sets letters to be a random line from letterCombinations.txt
	 * sets the centre letter to be a random one of those letters
	 * finds the score brackets based on the max score
	 */
	public SpellingBeeGame() {
		Random rand = new Random();
		List<String> letterCombos = getLetterCombos("DataFiles/letterCombinations.txt");
		letters = letterCombos.get(rand.nextInt(NUMLINES));
		centreLetter = letters.charAt(rand.nextInt(6));
		wordsFound = new ArrayList<String>();
		score = 0;
		bracket = getBrackets();
		System.out.println(getScore());
	}
	
	/*
	 * same as above but instead of finding a random set of letters
	 * it will use the given one
	 */
	public SpellingBeeGame(String letters) {
		Random rand = new Random();
		this.letters = letters;
		centreLetter = letters.charAt(rand.nextInt(8));
		wordsFound = new ArrayList<String>();
		score = 0;
		bracket = getBrackets();
	}
	
	/*
	 * returns a HashSet populated with lines from a file with the given path
	 * @param String path to file
	 * 
	 * @return HashSet where each element contains one line from the file
	 */
	public static HashSet<String> createWordsFromFile(String path){
		HashSet<String> allWords = new HashSet<String>();
		Path p = Paths.get(path);
		List<String> file;
		try {
			file = Files.readAllLines(p);
			for (String line : file) {
				allWords.add(line);
			}
		} catch (IOException e) {
			System.out.println("path is likely wrong");
			e.printStackTrace();
		}
		return allWords;
	}
	
	/*
	 * returns a HashSet populated with lines from a file with the given path
	 * @param String path to file
	 * 
	 * @return List  list containing all possible letter combos
	 */
	public static List<String> getLetterCombos(String path){
		Path p = Paths.get(path);
		List<String> file;
		try {
			file = Files.readAllLines(p);
			return file;
		} catch (IOException e) {
			System.out.println("path is likely wrong");
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * returns the amount of points a user should get for a given word
	 * 
	 * @param String the word to be evaluated
	 * 
	 * @return int the points received for the word
	 */
	public int getPointsForWord(String attempt) {
		if(attempt.length() == 4) {
			return 1;
		}

		else if (attempt.length() > 4 && attempt.length() < 7) {
			return attempt.length();
		}
		else if(attempt.length() >= 7) {
			if (isPanagram(attempt)) {
				return attempt.length()+7;
			}
			return attempt.length();
		}
		else {
			return 0;
		}
	}
	
	/*
	 * evaluates if a word is valid and if not what went wrong
	 * @param String word to be evaluated
	 * 
	 * @return String message representing the validity of the word
	 */
	public String getMessage(String attempt) {
		String message = "";
		if (!(attempt.matches("^["+this.letters+"]+$"))) {
			message = "you used letters you were not allowed to";
		}
		else if(attempt.length() < 4) {
			message = "This word is too short";
		}	
		else if (!ALLWORDS.contains(attempt)) {
			message = "This word does not exist in our dictionary";
		}
		else if (!(attempt.contains(String.valueOf(centreLetter)))) {
			message = "you must use your centre letter";
		}
		else if (wordsFound.contains(attempt)) {
			message = "you already entered this word";
		}
		else {
			message ="Good Word!";
			this.score +=getPointsForWord(attempt);
			wordsFound.add(attempt);
		}
		return message;
	}
	
	public String getMessageNoScore(String attempt) {
		String message = "";
		if (!(attempt.matches("^["+this.letters+"]+$"))) {
			message = "you used letters you were not allowed to";
		}
		else if(attempt.length() < 4) {
			message = "This word is too short";
		}	
		else if (!ALLWORDS.contains(attempt)) {
			message = "This word does not exist in our dictionary";
		}
		else if (!(attempt.contains(String.valueOf(centreLetter)))) {
			message = "you must use your centre letter";
		}
		else if (wordsFound.contains(attempt)) {
			message = "you already entered this word";
		}
		else {
			message ="Good Word!";
		}
		return message;
	}
	
	/*
	 * @return String letters the user may use
	 */
	public String getAllLetters() {
		return this.letters;
	}
	
	/*
	 * @return HashSet where each element contains one line from the file
	 */
	public char getCenterLetter() {
		return this.centreLetter;
	}
	
	/*
	 * @return HashSet where each element contains one line from the file
	 */
	public int getScore() {
		System.out.println(this.score);
		return this.score;
	}
	
	/*
	 * gets the score brackets the user may belong to
	 * 
	 * @return int[] each element represents a percentage of the max score and therefore the score brackets
	 */
	public int[] getBrackets() {
		int maxScore = getMaxScore();
		int[] brackets = new int[5];
		brackets[0] = (int) (0.25*maxScore);
		brackets[1] = (int) (0.5*maxScore);
		brackets[2] = (int) (0.75*maxScore);
		brackets[3] = (int) (0.9*maxScore);
		brackets[4] = maxScore;
		return brackets;
	}
	
	/*
	 * calculates the points for each possible word to arrive at the theoretical max points
	 * 
	 * @return int max score the user could achieve
	 */
	private int getMaxScore() {
		int maxScore = 0;
		for (String word : ALLWORDS) {
			if (getMessageNoScore(word).equals("Good Word!")) {
				maxScore += getPointsForWord(word);
			}
		}
	    return maxScore;
	}
	
	/*
	 * evaluates if a word contains all 7 letters that the user may use
	 * 
	 * @param String word to be evaluated
	 * 
	 * @return boolean whether or not the word is a panagram
	 */
	private boolean isPanagram(String word) {
		//this method is only called to calculate points which means that it necessarily only contains
		//letters the user may actually use. A hashset can only contain unique entries so here we can
		//check the length of a hashset representing the word and if it's length is 7 then it contains
		//7 unique letters and all of which the user is allowed to use meaning it is a panagram
		HashSet<Character> charSet = new HashSet<Character>();
		for (int i = 0; i < word.length(); i++) {
			charSet.add(word.charAt(i));
		}
		if (charSet.size() == 7) {
			return true;
		}
		else {
			return false;
		}
	}
}
