package spellingbee.client;

import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.beans.value.ChangeListener;
import spellingbee.network.Client;

/**
 * 
 * @author Mandeep Singh, Mishal Alarifi
 * 
 * This class launches the SpellingBeeClient as an application
 *
 */

public class SpellingBeeClient extends Application{
	
	//private Client object to communicate with server
	private Client clientObj = new Client();
	
	/**
	 * start is a method that loads the Application and creates the GUI
	 * @param stage The application window
	 * 
	 */
	public void start(Stage stage) {
		Group root = new Group();
		TabPane clientTabPane = new TabPane();
		Tab gameTab = new GameTab(this.clientObj);
		TextField score = ((GameTab) gameTab).getScoreField();
		Tab scoreTab = new ScoreTab(this.clientObj);
		score.textProperty().addListener(
				new ChangeListener<String>() {
					/*
					 * calls the refresh method to redraw the score tab 
					 * when the score changes
					 * 
					 */
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						((ScoreTab) scoreTab).refresh();
					}
				});
				
		clientTabPane.getTabs().add(gameTab);
		clientTabPane.getTabs().add(scoreTab);
		root.getChildren().add(clientTabPane);
		Scene scene = new Scene(root,650,300);
		stage.setTitle("Spelling Bee Client");
		stage.setScene(scene);
		stage.show();
		
		
	}
	/**
	 *  Main method that launches the application
	 */
	public static void main(String[]args) {
		Application.launch(args);
	}

	

}
